const mongoose = require("./db");

const { Schema } = mongoose;

const todoSchema = new Schema({
  data: {
    type: String,
    required: true,
  }, // String is shorthand for {type: String}
  isActive: {
    type: Boolean,
    default: false,
    require: false,
  },
});

module.exports = todoSchema;
