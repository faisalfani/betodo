const express = require("express");

const app = express();

const cors = require("cors");

const bodyParser = require("body-parser");

const port = process.env.PORT || 3000;

const todoList = require("./todoModel");

const getTodo = async () => {
  const todos = await todoList.find({});
  return todos;
};

const deleteTodo = async (id) => {
  const res = await todoList.deleteOne({ _id: id });
  return res;
};

const createTodo = async (data) => {
  const res = await todoList.create(data);
  return res;
};

const updateTodo = async (id) => {
  try {
    const getOne = await todoList.find({ _id: id });
    findResult = getOne[0];
    const msg = await todoList.updateOne(
      { _id: id },
      { isActive: !findResult.isActive }
    );
    return msg;
  } catch (error) {
    throw error;
  }
  // const res = await todoList.updateOne({_id : id}, {isActive :false})
};

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/", async (req, res) => {
  const todos = await getTodo();
  res.send(todos);
  // res.json(todos);
});

app.delete("/:id", async (req, res) => {
  const msg = await deleteTodo(req.params.id);
  if (msg.deletedCount == 1) {
    res.status(200);
    res.send("success deleted");
  } else {
    res.status(404);
    res.send("failed");
  }
});

app.post("/", async (req, res) => {
  try {
    const msg = await createTodo(req.body, res);
    res.status(201);
    res.send(msg);
  } catch (error) {
    res.status(406);
    res.send(error._message);
  }
});

app.patch("/:id", async (req, res) => {
  try {
    const msg = await updateTodo(req.params.id);
    if (msg.ok == 1) {
      res.status(201);
      res.send("success");
    }
  } catch (error) {
    res.send(error.message);
  }
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
