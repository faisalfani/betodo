const todoSchema = require("./todoSchema");
const mongoose = require("./db");

const todoList = mongoose.model("todolists", todoSchema);

module.exports = todoList;
